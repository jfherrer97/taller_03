# README #

Integrantes del grupo:
	Fabricio Herrera C.
	Steffany Vallejo C.

Detalles del proyecto:
	El repositorio debe de tener como mínimo 3 commits:
		Commit inicial donde se crea el README.md
		Commit donde se corrigen las observaciones y problemas encontrados en: Usar make para compilar un proyecto (este commit es opcional si no tuvo errores en esa tarea)
		Commit donde se crea una carpeta includes y se mueve point.h a esa carpeta, debe de actualizarse también el Makefile
		Commit donde se agrega una nueva función la cuál calcula el punto medio entre dos puntos (en main.c se debe de mostrar el resultado de esta función):
			Point punto_medio(Point a, Point b)
